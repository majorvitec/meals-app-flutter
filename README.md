# Meals App [2019]

Build a Meals App [Followed Tutorial]

## App Overview

### Meals Categories Screen

![Meals Categories Screen](/images/readme/overview_screen.png "Meals Categories Screen")

### Drawer Screen

![Drawer Screen](/images/readme/drawer_screen.png "Drawer Screen")

### Filters Screen

![Filters Screen](/images/readme/filters_screen.png "Filters Screen")

### Meals Screen

![Meals Screen](/images/readme/meals_screen.png "Meals Screen")

### Meals Details Screen

![Meals Details Screen](/images/readme/meals_details_screen.png "Meals Details Screen")
