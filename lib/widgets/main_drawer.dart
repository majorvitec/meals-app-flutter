import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Container createHeading(BuildContext context) {
    return Container(
      height: 120,
      width: double.infinity,
      padding: EdgeInsets.all(20),
      alignment: Alignment.centerLeft,
      color: Theme.of(context).colorScheme.secondary,
      child: Text(
        'Cooking Up!',
        style: TextStyle(
            fontWeight: FontWeight.w900,
            fontSize: 30,
            color: Theme.of(context).primaryColor),
      ),
    );
  }

  ListTile createButton(
      String title, IconData icon, void Function() tabHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tabHandler,
    );
  }

  void goToTabScreen(BuildContext context) {
    // replacing the prev page from the stack
    Navigator.of(context).pushReplacementNamed('/');
  }

  void goToFiltersScreen(BuildContext context) {
    // replacing the prev page from the stack
    Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          createHeading(context),
          SizedBox(height: 20),
          createButton(
            'Meal',
            Icons.restaurant,
            () => goToTabScreen(context),
          ),
          createButton(
            'Filters',
            Icons.settings,
            () => goToFiltersScreen(context),
          ),
        ],
      ),
    );
  }
}
