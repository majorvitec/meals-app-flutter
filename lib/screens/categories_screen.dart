import 'package:flutter/material.dart';

import '../widgets/category_item.dart';
import '../dummy-data.dart';

class CategoriesScreen extends StatelessWidget {
  List<Widget> createCategoryItems() {
    return DUMMY_CATEGORIES
        .map(
          (categoryData) => CategoryItem(
            categoryData.id,
            categoryData.title,
            categoryData.color,
          ),
        )
        .toList();
  }

  GridView createBody() {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: createCategoryItems(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return createBody();
  }
}
