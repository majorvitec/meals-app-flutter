import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  static const String routeName = '/filters';

  final bool glutenFree;
  final bool lactoseFree;
  final bool vegan;
  final bool vegetarian;
  final Function saveFilters;

  FiltersScreen({
    required this.glutenFree,
    required this.lactoseFree,
    required this.vegan,
    required this.vegetarian,
    required this.saveFilters,
  });

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = false;
  bool _lactoseFree = false;
  bool _vegan = false;
  bool _vegetarian = false;

  @override
  initState() {
    _glutenFree = widget.glutenFree;
    _lactoseFree = widget.lactoseFree;
    _vegan = widget.vegan;
    _vegetarian = widget.vegetarian;

    super.initState();
  }

  Widget createTitle() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'Adjust your meal selection',
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _createSwitch(
    String title,
    String subtitle,
    bool currentValue,
    void Function(bool) updateValue,
  ) {
    return SwitchListTile(
      title: Text(title),
      value: currentValue,
      subtitle: Text(subtitle),
      onChanged: updateValue,
    );
  }

  Widget createSwitches() {
    return Expanded(
      child: ListView(
        children: <Widget>[
          _createSwitch(
            'Gluten-free',
            'Only include gluten-free meals',
            _glutenFree,
            (newState) {
              setState(() {
                _glutenFree = newState;
              });
            },
          ),
          _createSwitch(
            'Lactose-free',
            'Only include lactose-free meals',
            _lactoseFree,
            (newState) {
              setState(() {
                _lactoseFree = newState;
              });
            },
          ),
          _createSwitch(
            'Vegetarian',
            'Only include vegetarian meals',
            _vegetarian,
            (newState) {
              setState(() {
                _vegetarian = newState;
              });
            },
          ),
          _createSwitch(
            'Vegan',
            'Only include Vegan meals',
            _vegan,
            (newState) {
              setState(() {
                _vegan = newState;
              });
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Your Filters'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                return widget.saveFilters(
                    _glutenFree, _lactoseFree, _vegan, _vegetarian);
              },
            ),
          ],
        ),
        drawer: MainDrawer(),
        body: Column(
          children: <Widget>[
            createTitle(),
            createSwitches(),
          ],
        ));
  }
}
