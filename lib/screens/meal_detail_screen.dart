import 'package:flutter/material.dart';

import '../dummy-data.dart';

class MealDetailScreen extends StatelessWidget {
  static const String routeName = '/meal-detail';

  final Function _toggleFavorite;
  final Function _isFavorite;

  MealDetailScreen(this._toggleFavorite, this._isFavorite);

  Container createImage(String imageUrl) {
    return Container(
      height: 300,
      width: double.infinity,
      child: Image.network(
        imageUrl,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget buildSectionTitle(BuildContext context, String title) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        title,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget createContainer(ListView child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 200,
      width: 300,
      child: child,
    );
  }

  Widget createIngredients(List ingredients) {
    return createContainer(
      ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            color: Theme.of(context).colorScheme.secondary,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(ingredients[index])),
          );
        },
        itemCount: ingredients.length,
      ),
    );
  }

  Widget createSteps(List steps) {
    return createContainer(
      ListView.builder(
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              ListTile(
                leading: CircleAvatar(
                  child: Text('# ${index + 1}'),
                ),
                title: Text(steps[index]),
              ),
              Divider(),
            ],
          );
        },
        itemCount: steps.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final String mealId = ModalRoute.of(context)!.settings.arguments as String;
    final selectedMeal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);

    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedMeal.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            createImage(selectedMeal.imageUrl),
            buildSectionTitle(context, 'Ingredients'),
            createIngredients(selectedMeal.ingredients),
            buildSectionTitle(context, 'Steps'),
            createSteps(selectedMeal.steps),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          this._isFavorite(mealId) ? Icons.star : Icons.star_border,
        ),
        onPressed: () => _toggleFavorite(mealId),
      ),
    );
  }
}
